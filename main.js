
// Опишіть своїми словами, що таке метод об'єкту - Метод об'єкту це функція яка є властивістю об’єкта.
// Який тип даних може мати значення властивості об'єкта? значення властивості об'єкта може бути будь-якого типу.
// Об'єкт це посилальний тип даних. Що означає це поняття? якщо по простому, то це як посилання не на певний елемент, а на якусь комірку в якій знаходиться елемент.

function createNewUser(
    userFirstName = prompt('Enter you name: ',''),
    userLastName = prompt('Enter you second name','')
) {
    
    this.firstName = userFirstName;

    this.lastName = userLastName;

    this.getLogin = function(){
        let newLogin = this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        return newLogin;
    }
}

let newUser = new createNewUser();

console.log(`Your login is: ${newUser.getLogin()}`);
console.log(newUser.firstName);
console.log(newUser.lastName);

alert(`Your login is: ${newUser.getLogin()}`);

